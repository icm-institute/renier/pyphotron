Installation
============

  - Create a suitable environment (python >=3.7 with at least the cython package)
  - Clone the repository
  - cd to the repository folder
  - run 


.. code-block:: sh

	python setup.py install


Running
=======


To get started, you can try the test function

.. code-block:: python

	>>> from pyphotron.pyphotron_pdclib import test
	>>> test()

You can see how the function is implemented to see how to use the library
